<?php
return [

    'password' => 'Heslo musí být dlouhé nejméně šest znaků a musí odpovídat potvrzení.',
    'reset' => 'Vaše heslo bylo resetováno!',
    'sent' => 'Váš odkaz na resetování hesla bylo odesláno na emailovou adresu!',
    'token' => 'Tento záznam pro obnovení hesla je neplatný.',
    'user' => 'Nelze najít uživatele s touto e-mailovou adresou.',

];
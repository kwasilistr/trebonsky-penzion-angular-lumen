export class Room {
  id: number;
  name: string;
  price: number;
  capacity: number;
  description: string;
  surface: number;
  parking: number;
  room_images: string;
}

import { Component } from '@angular/core';
import { ContactService} from "../../core/contact.service";
import { FormBuilder, Validators} from '@angular/forms';
import { Contact } from "../../contact";
import { Router } from "@angular/router";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PHONE_REGEX = /([+]?\d{1,3}[. \s]?)?(\d{9}?)/;
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent  {

  contact : Contact = {
      name: null,
      surname: null,
      email: null,
      phone: null,
      title: null,
      message: null
  };

  contactForm = this.fb.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        surname: ['', [Validators.required, Validators.minLength(3)]],
        email: ['', [Validators.required, Validators.pattern(EMAIL_REGEX)]],
        phone: ['', [Validators.required, Validators.pattern(PHONE_REGEX)]],
        title: ['', [Validators.required, Validators.minLength(3)]],
        message: ['', [Validators.required, Validators.minLength(25)]],
        checkbox: ['', [Validators.required],]
    });



  constructor(
      private fb: FormBuilder,
      private contactService: ContactService,
      private router: Router,
  ) { }

    public errorName:Array<any> = [
        {type:'required', msg:'Prosím vyplňte Jméno'},
    ];

    public errorSurname:Array<any> = [
        {type:'required', msg:'Prosím vyplňte Příjmení'},
    ];

    public errorEmail:Array<any> = [
        {type:'pattern', msg:'Prosím vyplňte správně email'},
        {type:'required', msg:'Zadejte email'},
    ];

    public errorTitle:Array<any> = [
        {type:'required', msg:'Prosím vyplňte Předmět'},
    ];

    public errorPhone:Array<any> = [
        {type:'required', msg:'Prosím vyplňte Telefon'},
    ];

    public errorMessage:Array<any> = [
        {type:'required', msg:'Prosím vyplňte Vaši zprávu'},
    ];


    submit() {
      if(this.contactForm.valid){
          // console.log(this.contactForm);
          console.log(this.contact);

      this.contactService.addContact(this.contact).subscribe((data: Contact[])=> {
          alert('Vaše zpráva byla odeslána');
          console.log(data);
          this.contactForm.reset();
          this.router.navigateByUrl('home');
      }, (error) => {
          console.log(error);
      })
    }

 }
}
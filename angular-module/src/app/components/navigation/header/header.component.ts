import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  languages = ['cz', 'en', 'de'];

  @Output()
  SideNavigationToggle = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onToggleOpenSidenav() {
    this.SideNavigationToggle.emit();
  }


}

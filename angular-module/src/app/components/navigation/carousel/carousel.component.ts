import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  slides = [
    {
      img: './assets/images/carousel/carousel-1.jpg'
    },
    {
      img: './assets/images/carousel/carousel-2.jpg'
    },
    {
      img: './assets/images/carousel/carousel-3.jpg'
    },
    {
      img: './assets/images/carousel/carousel-4.jpg'
    },
    {
      img: './assets/images/carousel/carousel-5.jpg'
    }
  ];
  slideConfig = {
    arrows: true,
    dots: false,
    infinite: false,
    speed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    // console.log('afterChange');
  }

  beforeChange(e) {
    // console.log('beforeChange');
  }

  constructor() {}

  ngOnInit() {}
}

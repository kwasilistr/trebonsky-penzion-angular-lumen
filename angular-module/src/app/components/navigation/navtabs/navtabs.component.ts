import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navtabs',
  templateUrl: './navtabs.component.html',
  styleUrls: ['./navtabs.component.scss']
})
export class NavtabsComponent implements OnInit {
  navigation = [
    { link: 'home', label: 'Domů' },
    { link: 'accomodation', label: 'Ubytování' },
    { link: 'contact', label: 'Kontakt' },
    // { link: 'booking', label: 'Rezervace'}
  ];

  constructor() {}

  ngOnInit() {}
}

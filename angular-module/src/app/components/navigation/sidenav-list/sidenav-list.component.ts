import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {
  navigation = [
    { link: 'home', label: 'Domů', icon: 'home' },
    { link: 'accomodation', label: 'Ubytování', icon: 'hotel' },
    { link: 'contact', label: 'Kontakt', icon: 'perm_phone_msg' },
  ];

  @Output()
  closeSideNavigation = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onToggleClose() {
    this.closeSideNavigation.emit();
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kitchenette-carousel',
  templateUrl: './kitchenette-carousel.component.html',
  styleUrls: ['./kitchenette-carousel.component.css']
})
export class KitchenetteCarouselComponent implements OnInit {
    slides = [
        {
            img: '../../../../assets/images/rooms/kitchenette/kitchenette-1.jpg'
        },
        {
            img: '../../../../assets/images/rooms/kitchenette/kitchenette-2.jpg'
        },
        {
            img: '../../../../assets/images/rooms/kitchenette/kitchenette-3.jpg'
        },
        {
            img: '../../../../assets/images/rooms/kitchenette/kitchenette-4.jpg'
        },
        {
            img: '../../../../assets/images/rooms/kitchenette/kitchenette-5.jpg'
        }
    ];
    slideConfig = {
        arrows: true,
        dots: true,
        infinite: false,
        speed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        // responsive: [
        //     {
        //         breakpoint: 1024,
        //         settings: {
        //             slidesToShow: 1,
        //             slidesToScroll: 1,
        //             infinite: true,
        //             dots: false
        //         }
        //     },
        //     {
        //         breakpoint: 600,
        //         settings: {
        //             slidesToShow: 1,
        //             slidesToScroll: 1
        //         }
        //     },
        //     {
        //         breakpoint: 480,
        //         settings: {
        //             slidesToShow: 1,
        //             slidesToScroll: 1
        //         }
        //     }
        // ]
    };


    constructor() {}

    ngOnInit() {}
}

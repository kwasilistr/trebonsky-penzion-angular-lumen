import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Room } from '../../../room';
import { RoomService } from '../../../core/room.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: [ './room-detail.component.css' ]
})
export class RoomDetailComponent implements OnInit {
  @Input() room: Room;

    roomForm = this.fb.group({
        checkIn: ['', Validators.required],
        checkOut: ['', Validators.required],
        adults: ['', Validators.required],
        children: ['']
    });

  constructor(
    private route: ActivatedRoute,
    private roomService: RoomService,
    private location: Location,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getRoom();
      console.log();
  }

  getRoom(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.roomService.getRoom(id)
      .subscribe(room => this.room = room);
  }

  goBack(): void {
    this.location.back();
  }

  submit() {
      console.log(this.roomForm.value);

  }

}

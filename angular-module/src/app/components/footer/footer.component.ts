import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  navigation = [
    { link: 'home', label: 'Domů', icon: 'home' },
    { link: 'accomodation', label: 'Ubytování', icon: 'hotel'  },
    { link: 'contact', label: 'Kontakt', icon: 'contact_phone'  }

  ];
  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-booking-step-1',
  templateUrl: './booking-step-1.component.html',
  styleUrls: ['./booking-step-1.component.css']
})
export class BookingStep1Component implements OnInit {
  public form: FormGroup;
  public roomList: FormArray;
  roomControl = new FormControl('', [Validators.required]);
  selectFormControl = new FormControl('', Validators.required);

  constructor(private fb: FormBuilder) {

  }


  get roomsFormGroup() {
    return this.form.get('rooms') as FormArray;
  }


  ngOnInit() {
    this.form = this.fb.group({
      checkIn: ['', Validators.required],
      checkOut: ['', Validators.required],
      roomsCount: [null],
      rooms: this.fb.array([]),
      guests: [null],
    });



    // set roomlist to this field
    this.roomList = this.form.get('rooms') as FormArray;
    this.onChanges();
  }


  createRoom(): FormGroup {
    return this.fb.group({
      adults: [null, Validators.required],
      children: [null]
    });
  }

  onChanges(): void {
    // this.heroBookingPanelForm.get('adults').valueChanges.subscribe( value => {console.log('počet dospělých: ' + value); });
    this.form.get('roomsCount').valueChanges.subscribe(val => {
      console.log('vybrano pokoju: ' + val);
      const diff = this.roomList.length - val;
      if (diff < 0) {
        let i;
        for (i = 0; i < Math.abs(diff); i++) {
          this.roomList.push(this.createRoom());
        }
      } else if (diff > 0) {
        // ubrat
        let k;
        for (k = 0; k < Math.abs(diff); k++) {
          this.roomList.removeAt(this.roomList.length - 1);
        }
      }

      console.log('diff: ' + diff + ', roomlist.length: ' + this.roomList.length);
    });
  }

  submit() {
    console.log(this.form.value);
  }
  editForm() {

  }




}



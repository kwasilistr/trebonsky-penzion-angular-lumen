import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
// import { TestService} from "../../test.service";

@Component({
  selector: 'app-booking-panel',
  templateUrl: './booking-panel.component.html',
  styleUrls: ['./booking-panel.component.css']
})
export class BookingPanelComponent implements OnInit {

  public form: FormGroup;

  rooms: any;

  constructor(

      private fb: FormBuilder,
      private router: Router,
      private http: HttpClient
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      checkIn: ['', Validators.required],
      checkOut: ['', Validators.required],
      adults: ['', Validators.required],
      children: [''],
    });

  }
  // getRooms() {
  //   this.testService
  //       .getRooms()
  //       .subscribe(
  //           room => this.rooms = room,
  //           // error => this.errorMessage = <any>error
  //       );
  // }
  submit() {
    console.log(this.form.value);
    if (this.form.valid) {
     this.router.navigateByUrl('booking');
      this.form.reset();

    }
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { OktaAuthModule, OktaCallbackComponent } from '@okta/okta-angular';
import { CustomIconService } from './core/custom-icon.service';

import { SharedModule } from './shared';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { RoomDetailComponent } from './components/rooms/room-detail/room-detail.component';
import { RoomsComponent } from './components/rooms/rooms.component';
import { HeaderComponent } from './components/navigation/header/header.component';
import { NavtabsComponent } from './components/navigation/navtabs/navtabs.component';
import { SidenavListComponent } from './components/navigation/sidenav-list/sidenav-list.component';
import { ContactComponent } from './components/contact/contact.component';
import { FooterComponent } from './components/footer/footer.component';
import { CarouselComponent } from './components/navigation/carousel/carousel.component';
import { BookingPanelComponent } from './components/booking-panel/booking-panel.component';
import { KitchenetteCarouselComponent } from './components/rooms/kitchenette-carousel/kitchenette-carousel.component';
import { MAT_DATE_LOCALE } from "@angular/material/core";

const oktaConfig = {
    issuer: '{https://dev-802124.okta.com/oauth2/default}',
    redirectUri: 'http://localhost:4200/implicit/callback',
    clientId: '{0oacucnspv59CHtzT356}'
};

import { BookingStep1Component} from "./components/booking-form/components/booking-step-1.component";
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    OktaAuthModule.initAuth(oktaConfig)
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    NavtabsComponent,
    SidenavListComponent,
    CarouselComponent,
    BookingPanelComponent,
    HomeComponent,
    RoomsComponent,
    RoomDetailComponent,
    ContactComponent,
    KitchenetteCarouselComponent,
    FooterComponent,
    BookingStep1Component,
    PrivacyPolicyComponent
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'cs-CZ'},
      CustomIconService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

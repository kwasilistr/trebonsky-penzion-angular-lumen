export class Contact {
    name: string;
    surname: string;
    email: string;
    title: string;
    phone: string;
    message: string;
}

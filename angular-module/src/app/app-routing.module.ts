import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }        from './components/home/home.component';
import { RoomsComponent }       from './components/rooms/rooms.component';
import { RoomDetailComponent }  from './components/rooms/room-detail/room-detail.component';
import { ContactComponent }     from "./components/contact/contact.component";
import { BookingStep1Component } from "./components/booking-form/components/booking-step-1.component";
import { PrivacyPolicyComponent} from "./components/privacy-policy/privacy-policy.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'accomodation', component: RoomsComponent },
  { path: 'room/:id', component: RoomDetailComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'booking', component: BookingStep1Component },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,  { useHash: false }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Room } from '../room';
import { MessageService } from './message.service';


@Injectable({ providedIn: 'root' })
export class RoomService {

//  private roomsUrl = 'https://api.trebonsky-penzion.cz/rooms';  // URL to web api
  private roomsUrl = 'http://localhost:8000/rooms';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }


  /** GET rooms from the server */
  getRooms (): Observable<Room[]> {
    return this.http.get<Room[]>(this.roomsUrl)
      .pipe(
        tap(_ => this.log('fetched rooms')),
        catchError(this.handleError('getRooms', []))
      );
  }

  /** GET room by id. Return `undefined` when id not found */
  getRoomNo404<Data>(id: number): Observable<Room> {
    const url = `${this.roomsUrl}/?id=${id}`;
    return this.http.get<Room[]>(url)
      .pipe(
        map(rooms => rooms[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} room id=${id}`);
        }),
        catchError(this.handleError<Room>(`getRoom id=${id}`))
      );
  }

  /** GET room by id. Will 404 if id not found */
  getRoom(id: number): Observable<Room> {
    const url = `${this.roomsUrl}/${id}`;
    return this.http.get<Room>(url).pipe(
      tap(_ => this.log(`fetched room id=${id}`)),
      catchError(this.handleError<Room>(`getRoom id=${id}`))
    );
  }

  /* GET rooms whose name contains search term */
  searchRooms(term: string): Observable<Room[]> {
    if (!term.trim()) {
      // if not search term, return empty room array.
      return of([]);
    }
    return this.http.get<Room[]>(`${this.roomsUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`found rooms matching "${term}"`)),
      catchError(this.handleError<Room[]>('searchRooms', []))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a RoomService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`RoomService: ${message}`);
  }
}

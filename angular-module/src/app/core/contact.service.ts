import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Contact } from '../contact';



@Injectable({ providedIn: 'root'})

export class ContactService {

    // contactUrl = 'https://api.trebonsky-penzion.cz/contact'; // URL to web api
    contactUrl = 'http://localhost:8000/contact'; // URL to web api

    constructor(private httpClient: HttpClient) { }

    addContact(contact: Contact) {

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this.httpClient.post(this.contactUrl, contact, {headers: headers}) ;


    }

}

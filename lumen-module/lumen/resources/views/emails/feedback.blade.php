<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div id="mailsub" class="notification" align="center">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;">
        <tr>
            <td align="center" bgcolor="#eff3f8">
                <!--[if gte mso 10]>
                <table width="680" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                <![endif]-->

                <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%"
                       style="max-width: 680px; min-width: 300px;">
                    <tr>
                        <td>
                            <!-- padding -->
                            <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                        </td>
                    </tr>
                    <!--header -->
                    <tr>
                        <td align="center" bgcolor="#ffffff">
                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left">

</div>
</td>
<td align="center">
    <![endif]-->
    <div class="mob_center_bl" style="float: none; display: inline-block; width: 88px;">
        <table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
            <tr>
                <td align="center" valign="middle">
                    <!-- padding -->
                    <div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center">
                                <!--Logo -->
                                <div class="mob_center_bl" style="width: 88px;">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="30" align="center" style="line-height: 19px;">
                                                <a href="#" target="_blank"
                                                   style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                    <font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
                                                        <img src="https://trebonsky-penzion.cz/assets/images/trebonsky-penzion-logo.png"
                                                             width="117" height="93" alt="Třeboňský Penzion" border="0"
                                                             style="display: block;"/>
                                                    </font>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--Logo END-->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div><!-- Item END-->
</td>
</tr>
</table>
<!-- padding -->
</td></tr>
<!--header END-->

<!--content -->
<tr>
    <td align="center" bgcolor="#fbfcfd">
        <table width="90%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center">
                    <!-- padding -->
                    <div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                    <div style="line-height: 44px;">
                        <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
						Dobrý den
					</span></font>
                    </div>
                    <!-- padding -->
                    <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div style="line-height: 24px;">
                        <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
					Tento e-mail vám zasíláme na základě vyplnění kontaktního formuláře<br>
                    na stránkách www.trebonsky-penzion.cz<br>
                    V případě dotazů nás prosím kontaktujte na info@trebonsky-penzion.cz.
					</span></font>
                    </div>
                    <!-- padding -->
                    <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div style="line-height: 24px;">
                        <h3>{{ $contact['name'] }} {{ $contact['surname'] }}</h3>
                        <h3>{{ $contact['phone'] }}.</h3>
                        <h3>{{ $contact['email'] }}</h3>
                        <h3>{{ $contact['title'] }}.</h3>
                        <h3>{{ $contact['message'] }}.</h3>
                    </div>
                    <!-- padding -->
                    <div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                </td>
            </tr>
        </table>
    </td>
</tr>
<!--content END-->

<!--footer -->
<tr>
    <td class="iage_footer" align="center" bgcolor="#ffffff">
        <!-- padding -->
        <div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center">
                    <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
				<span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
					2019 © Třeboňský penzion.
				</span></font>
                </td>
            </tr>
        </table>

        <!-- padding -->
        <div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
    </td>
</tr>
<!--footer END-->
<tr>
    <td>
        <!-- padding -->
        <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
    </td>
</tr>
</table>
<!--[if gte mso 10]>
</td></tr>
</table>
<![endif]-->

</td></tr>
</table>

</div>

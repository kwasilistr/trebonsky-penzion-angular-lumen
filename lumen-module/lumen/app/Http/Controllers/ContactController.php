<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackMail;
use App\ContactModel;

class ContactController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $contact = new ContactModel($request->all());
//        Mail::to('app@trebonsky-penzion.cz')->send(new FeedbackMail($contact));
        $this->email($request);
        $contact->save();
        return response()->json('Uloženo');
    }

    public function email(Request $request)
    {

        $contact = new ContactModel($request->all());
        Mail::to('info@trebonsky-penzion.cz')
            ->cc($contact['email'])
            ->send(new FeedbackMail($contact));
    }
}




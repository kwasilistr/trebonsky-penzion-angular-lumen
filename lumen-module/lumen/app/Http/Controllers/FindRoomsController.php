<?php

namespace App\Http\Controllers;

use App\BookingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class FindFreeRoomsController extends Controller
{
    public function index()
    {
        return BookingModel::all('id', 'room', 'arrival', 'departure');
    }

    public function show($id)
    {
        return BookingModel::all()->where('room', $id)->all();
    }
}

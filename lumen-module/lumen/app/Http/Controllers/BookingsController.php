<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookingModel;

class RoomsController extends Controller
{
    public function index()
    {
        return BookingModel::with('roomImages')->get();

    }

    public function show($id)
    {
        return BookingModel::with('roomImages')->where('id', $id)->first();
    }

















    public function store(Request $request)
    {
        $room = RoomModel::create($request->all());

        return response()->json($room, 201);
    }

    public function update(Request $request, RoomModel $room)
    {
        $room->update($request->all());

        return response()->json($room, 200);
    }

    public function delete(RoomModel $room)
    {
        $room->delete();

        return response()->json(null, 204);
    }
}

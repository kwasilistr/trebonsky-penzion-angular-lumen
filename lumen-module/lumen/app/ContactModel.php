<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
    protected $fillable = [
        'name',
        'surname',
        'email',
        'phone',
        'title',
        'message'
    ];
    protected $table = 'contact_form';
}

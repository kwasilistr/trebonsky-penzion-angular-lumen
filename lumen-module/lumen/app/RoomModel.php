<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RoomModel extends Model
{
    protected $fillable = [
        'id',
        'name',
        'price',
        'capacity',
        'description',
        'surface',
    ];
    protected $table = 'room';

    /**
     * Get the images for the room.
     */
    public function roomImages()
    {
        return $this->hasMany('App\RoomImageModel' , 'room_id');
    }

}

<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ResourceModel extends Model
{

    public function roomImages()
    {
        return $this->belongsTo('App\RoomImageModel');
    }

}

<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RoomImageModel extends Model
{
    protected $fillable = [
        'id',
        'room_id',
        'img',
    ];
    protected $table = 'room_images';
//
    public $baseUrl = './assets/images/rooms';

    public function Images()
    {
        return $this->belongsTo('App\RoomModel');
    }
}

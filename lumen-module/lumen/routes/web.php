<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//
$router->get('/', function () {
    return view ('welcome');
});

$router->get('/rooms', 'RoomsController@index');
$router->get('rooms/{rooms}', 'RoomsController@show');

//Route::post('rooms', 'RoomsController@store');
//Route::put('rooms/{rooms}', 'RoomsController@update');
//Route::delete('rooms/{rooms}', 'RoomsController@delete');

/* --- Contact --- */

$router->post('contact', 'ContactController@store');
$router->get('contact', 'ContactController@email');
